# Sicredi Desafio QE - API Rest-Assured

## Pré-requisito
* Java JDK 21
* Maven

## Como executar os testes

Você pode abrir a classe de teste localizada em `src\main\java\br.sicredi.rest.suite` e executar a `SuiteTest.class`

### Relatório de teste

  Tentei configurar o Allure Report para gerar automaticamente o relatório de teste, mas não obtive sucesso. O relatório é gerado vazio:
Configurações realizadas:
* configuração do aspectoj no arquivo `pom.xml`
* Arquivo `allure.properties` em `src/main/resources`

## Bibliotecas
* [RestAssured](http://rest-assured.io/) biblioteca para testar APIs REST
* [Allure Report](https://docs.qameta.io/allure/) gerar relatório de testes

## Casos de Testes - Sucesso
![img.png](img.png)

## Bugs e Melhorias

![img_1.png](img_1.png)