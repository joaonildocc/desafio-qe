package br.sicredi.rest.core;

import io.restassured.http.ContentType;

public interface Constantes {
    String Base_URL = "https://dummyjson.com";
    int PORTA = 443;
    String BASE_PÁTH = "";

    ContentType CONTENT_TYPE = ContentType.JSON;

    Long MAX_TIMEOUT = 5000L;

    int SC_SUCCESS = 200;
    int SC_CREATED = 201;

    int SC_BAD_REQUEST = 400;
    int SC_UNAUTHORIZED = 401;
    int SC_FORBIDDEN = 403;

    int SC_NOT_FOUND = 404;
}
