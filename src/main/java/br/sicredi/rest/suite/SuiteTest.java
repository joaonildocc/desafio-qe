package br.sicredi.rest.suite;

import br.sicredi.rest.core.BaseTest;
import br.sicredi.rest.tests.AuthTest;
import br.sicredi.rest.tests.ProdutoTest;
import br.sicredi.rest.tests.UsuarioTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static io.restassured.RestAssured.given;

@RunWith(org.junit.runners.Suite.class)
@Suite.SuiteClasses({
        UsuarioTest.class,
        AuthTest.class,
        ProdutoTest.class,
})
public class SuiteTest extends BaseTest {

    @Test
    public void healthTest() {
        given()
        .when()
            .get("/test")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }


}

