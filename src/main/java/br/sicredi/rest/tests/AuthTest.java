package br.sicredi.rest.tests;

import br.sicredi.rest.core.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class AuthTest extends BaseTest {

    @Test
    public void CadastrarTokenUsuario() {


        given()
                .body("{\"username\": \"atuny0\", \"password\": \"9uQFF1Lh\"}")
        .when()
                .post("/auth/login")
        .then()
                .statusCode(SC_CREATED);

        /*BUG 01:  Ao enviar a request POST /auth/login de acordo com a documentação o response
        deve retornar o status code 201, porém é retornado sucesso com status code 200.
        Sugiro corrigir o retorno da API, tendo em vista que está sendo criado um recurso(token) e deve ser retornado 201.*/

        ;
    }

    @Test
    public void CadastrarTokenUsuarioPasswordIncorreto() {

        given()
                .body("{\"username\": \"atuny0\", \"password\": \"INCORRETO\"}")
        .when()
                .post("/auth/login")
        .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void CadastrarTokenUsuarioUserNameIncorreto() {

        given()
                .body("{\"username\": \"atuny0\", \"INCORRETO\": \"9uQFF1Lh\"}")
        .when()
                .post("/auth/login")
        .then()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void CadastrarTokenUsuarioDadosURI() {

        given()
        .when()
                .post("/auth/login/filter?key=username&value=kminchelle&key=password&value=0lelplR")
        .then()
                .statusCode(SC_FORBIDDEN);
    }

}
