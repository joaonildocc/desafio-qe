package br.sicredi.rest.tests;

import br.sicredi.rest.core.BaseTest;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ProdutoTest extends BaseTest {

    private String TOKEN;

    @Before
    public void auth() {
        Map<String, String> login = new HashMap<>();
        login.put("username", "kminchelle");
        login.put("password", "0lelplR");

        TOKEN = given()
                .body(login)
        .when()
                .post("/auth/login")
        .then()
                .statusCode(SC_SUCCESS)
                .extract().path("token");

    }
    @Test
    public void listarProdutosComTokenSucesso() {

        given()
                .header("Authorization", TOKEN)
        .when()
                .get("/auth/products")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }


    @Test
    public void ListarProdutosComTokenInvalido() {

        given()
                .header("Authorization", "TOKEN")
        .when()
                .get("/auth/products")
        .then()
                .statusCode(SC_UNAUTHORIZED);
        ;
    }

    @Test
    public void ListarProdutosSemToken() {
        given()
        .when()
                .get("/auth/products")
        .then()
                .statusCode(SC_FORBIDDEN)
        ;
    }

    @Test
    public void cadastrarNovoProdutoSucesso() {
        Produto produto = getProdutoMock();

        given()
                .body(produto)
        .when()
                .post("/products/add")
        .then()
                .statusCode(SC_CREATED);

        /*BUG 02: Ao enviar a request POST /products/add de acordo com a documentação o response
        deve retornar o status code 201, porém é retornado sucesso com status code 200.
        Sugiro corrigir o retorno da API, tendo e+A1:H21m vista que está sendo criado um recurso(produto novo) e deve ser retornado 201.*/
        ;
    }

    @Test
    public void cadastrarNovoProdutoEmBranco() {


        given()
                .body("{}")
        .when()
                .post("/products/add")
        .then()
                .statusCode(SC_CREATED);
        ;
    }

    @Test
    public void cadastrarNovoProdutoTitle() {

        given()
                .body("{\"title\": \"TV Samsung\"}")
        .when()
                .post("/products/add")
        .then()
                .statusCode(SC_CREATED);
        ;
    }


    @Test
    public void ListarTodosProdutos() {


        given()
        .when()
                .get("/products")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }

    @Test
    public void ListarProdutoCadastrado() {


        given()
        .when()
                .get("/products/101")
        .then()
                .statusCode(SC_NOT_FOUND);

        /*Não será considerado bug, devido a NOTA da documentação informando que os produtos
        criados não foram persistidos. Nesse caso, é esperado que não seja retornado! Foi inserido o Status Code 404 para validação e não 200 */
        ;
    }

    @Test
    public void ListarProdutoInexistente() {


        given()
                .when()
                .get("/products/0")
                .then()
                .statusCode(SC_NOT_FOUND);

        /*Não será considerado bug, devido a NOTA da documentação informando que os produtos
        criados não foram persistidos. Nesse caso, é esperado que não seja retornado!*/
        ;
    }



    private Produto getProdutoMock(){

        Produto produto = new Produto();

        produto.setTitle("Perfume Oil");
        produto.setDescription("Mega Discount, Impression of A...");
        produto.setPrice(40.00F);
        produto.setDiscountPercentage(8.4F);
        produto.setRating(4.26F);
        produto.setStock(65);
        produto.setBrand("Impression of Acqua Di Gio");
        produto.setCategory("fragrances");
        produto.setThumbnail("https://i.dummyjson.com/data/products/11/thumnail.jpg");
        return produto;

    }

}
