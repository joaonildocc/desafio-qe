package br.sicredi.rest.tests;

import br.sicredi.rest.core.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class UsuarioTest extends BaseTest {

    @Test
    public void listarTodosUsuariosComSucesso() {

        given()
        .when()
                .get("/users")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }

    @Test
    public void listarUsuarioFiltroUrlComSucesso() {

        given()
        .when()
                .get("/users/5")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }
    @Test
    public void listarTodosUsuariosUserIdComSucesso() {

        given()
        .when()
                .get("/users/10/todos")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }

    @Test
    public void listarTodosUsuariosEyeColorGreenComSucesso() {

        given()
        .when()
                .get("/users/filter?key=eyeColor&value=Green")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }

    @Test
    public void listarUsuarioPorUserNameComSucesso() {

        given()
        .when()
                .get("/users/filter?key=username&value=kminchelle")
        .then()
                .statusCode(SC_SUCCESS);
        ;
    }
    @Test
    public void listarUsuarioInexistente() {

        given()
        .when()
                .get("/users/0")
        .then()
                .statusCode(SC_NOT_FOUND);
        ;
    }



}
